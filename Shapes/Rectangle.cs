﻿using System;

namespace Shapes {
    class Rectangle : Shape {

        public double width { get; set; }
        public double height { get; set; }

        public Rectangle(double width, double height) {
            this.width = width;
            this.height = height;
        }

        public Rectangle(double height) {
            width = height;
            this.height = height;
        }

        public override double Area() {
            double area = width * height;
            return Math.Round(area, 3);
        }
    }
}
