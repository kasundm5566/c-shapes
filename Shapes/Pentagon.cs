﻿using System;

namespace Shapes {
    class Pentagon : Shape {
        public double sideSize { get; set; }

        public Pentagon(double sideSize) {
            this.sideSize = sideSize;
        }

        public override double Area() {
            double area = Math.Sqrt(5 * (5 + 2 * Math.Sqrt(5))) / 4 * Math.Pow(sideSize, 2);
            return Math.Round(area, 3);
        }
    }
}
