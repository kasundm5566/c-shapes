﻿namespace Shapes {
    class Square : Rectangle {

        public Square(double height) : base(height) {
        }

        public static Square operator +(Square square1, Square square2) {
            double square1Height = square1.height;
            double square2Height = square2.height;
            Square combinedSquare = new Square(square1.height + square2.height);
            return combinedSquare;
        }
    }
}
