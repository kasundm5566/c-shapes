﻿using System;

namespace Shapes {
    class Triangle : Shape {
        public double sideSize { get; set; }

        public Triangle(double sideSize) {
            this.sideSize = sideSize;
        }

        public override double Area() {
            double area = Math.Sqrt(3) / 4 * Math.Pow(sideSize, 2);
            return Math.Round(area, 3);
        }

        public static Triangle operator +(Triangle triangle1,Triangle triangle2) {
            double triangle1SideSize = triangle1.sideSize;
            double triangle2SideSize = triangle2.sideSize;
            Triangle combinedTriangle = new Triangle(triangle1SideSize + triangle2SideSize);
            return combinedTriangle;
        }
    }
}
