﻿using System;

namespace Shapes {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("AREAS OF THE SHAPES");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-------------------");
            Console.ResetColor();

            Rectangle rectangle = new Rectangle(4, 8);
            Console.WriteLine("Rectangle: {0}", rectangle.Area());

            Square square = new Square(5);
            Console.WriteLine("Square: {0}", square.Area());

            Circle circle = new Circle(7);
            Console.WriteLine("Circle: {0}", circle.Area());
            Console.WriteLine("Circle circumference: {0}", circle.Circumference());

            Triangle triangle = new Triangle(5);
            Console.WriteLine("Triangle: {0}", triangle.Area());

            Pentagon pentagon = new Pentagon(5);
            Console.WriteLine("Pentagon: {0}", pentagon.Area());

            Hexagon hexagon = new Hexagon(3);
            Console.WriteLine("Hexagon: {0}", hexagon.Area());

            Console.WriteLine("\nOBJECT ADDITION");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("----------------");
            Console.ResetColor();

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Triangle");
            Console.ResetColor();
            Triangle triangle1 = new Triangle(2);
            Triangle triangle2 = new Triangle(3);
            Triangle generatedTriangle = triangle1 + triangle2;
            Console.WriteLine("First object size: {0}", triangle1.sideSize);
            Console.WriteLine("Second object size: {0}", triangle2.sideSize);
            Console.WriteLine("Generated object size: {0}", generatedTriangle.sideSize);
            Console.WriteLine("Generated object area: {0}", generatedTriangle.Area());

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\nCircle");
            Console.ResetColor();
            Circle circle1 = new Circle(2);
            Circle circle2 = new Circle(4);
            Circle generatedCircle = circle1 + circle2;
            Console.WriteLine("First object size: {0}", circle1.radius);
            Console.WriteLine("Second object size: {0}", circle2.radius);
            Console.WriteLine("Generated object size: {0}", generatedCircle.radius);
            Console.WriteLine("Generated object area: {0}", generatedCircle.Area());

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\nSquare");
            Console.ResetColor();
            Square square1 = new Square(1);
            Square square2 = new Square(3);
            Square generatedSquare = square1 + square2;
            Console.WriteLine("First object size: {0}", square1.height);
            Console.WriteLine("Second object size: {0}", square2.height);
            Console.WriteLine("Generated object size: {0}", generatedSquare.height);
            Console.WriteLine("Generated object area: {0}", generatedSquare.Area());

            Console.ReadLine();
        }
    }
}
