﻿using System;

namespace Shapes {
    class Circle : Shape {
        public double radius { get; set; }
        const double pi = 3.14159;

        public Circle(double radius) {
            this.radius = radius;
        }

        public override double Area() {
            double area = pi * Math.Pow(radius, 2);
            return Math.Round(area, 3);
        }

        public double Circumference() {
            return 2 * pi * radius;
        }

        public static Circle operator +(Circle circle1, Circle circle2) {
            double circle1Radius = circle1.radius;
            double circle2Radius = circle2.radius;
            Circle combinedCircle = new Circle(circle1Radius + circle2Radius);
            return combinedCircle;
        }
    }
}
