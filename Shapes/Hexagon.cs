﻿using System;

namespace Shapes {
    class Hexagon : Shape {
        public double sideSize { get; set; }

        public Hexagon(double sideSize) {
            this.sideSize = sideSize;
        }

        public override double Area() {
            double area = 3 * Math.Sqrt(3) / 2 * Math.Pow(sideSize, 2);
            return Math.Round(area, 3);
        }
    }
}
