﻿namespace Shapes {
    abstract class Shape {
        public abstract double Area();
    }
}
